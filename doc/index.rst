.. ftrack-connect-pipeline documentation master file, created by
   sphinx-quickstart on Tue Dec  1 12:12:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ftrack-connect-pipeline's documentation!
===================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   developing/index
   release/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
