..
    :copyright: Copyright (c) 2022 ftrack

.. _release/release_notes:

*************
Release Notes
*************

.. release:: upcoming

    .. change:: changed
        :tags: context

        Rewired the context event flow to support standalone delayed context set

    .. change:: changed
        :tags: doc

        Added release notes and API documentation

    .. change:: changed
        :tags: utils

        Added shared safe_string util function

    .. change:: changed
        :tags: doc

        Fixed AM client docstrings

.. release:: 1.0.1
    :date: 2022-08-01

    .. change:: new

        Initial release

